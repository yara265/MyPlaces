package com.example.compiler;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.compiler.database.DataBase;
import com.example.compiler.model.User;

public class Signup extends Activity {

	EditText email;
	EditText name;
	EditText password;
	Button save;
	Context con = this;
	DataBase dp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		dp = new DataBase(con);
		email = (EditText) findViewById(R.id.semail);
		password = (EditText) findViewById(R.id.spassword);
		name = (EditText) findViewById(R.id.sname);
		save = (Button) findViewById(R.id.ssave);
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				User user = new User();
				user.setEmail(email.getText().toString());
				user.setName(name.getText().toString());
				user.setPassword(password.getText().toString());
				dp.sign(user);
				Toast.makeText(con, "saved succsessfully",
						500 * Toast.LENGTH_LONG).show();
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signup, menu);
		return true;
	}

}
