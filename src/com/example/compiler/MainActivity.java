package com.example.compiler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.compiler.database.DataBase;
import com.example.compiler.model.User;

public class MainActivity extends Activity {

	Button login;
	Button signup;
	Context con = this;
	EditText email;
	EditText password;
	DataBase dp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dp = new DataBase(con);

		login = (Button) findViewById(R.id.button1);
		signup = (Button) findViewById(R.id.button2);
		email = (EditText) findViewById(R.id.editText1);
		password = (EditText) findViewById(R.id.editText2);

		login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String emailValue = email.getText().toString();
				String passwordValue = password.getText().toString();
				User user = dp.login(emailValue, passwordValue);
				if (user != null) {
					Intent i = new Intent(con, Logined.class);
					i.putExtra("email", user.getEmail());
					i.putExtra("name", user.getName());
					i.putExtra("id", user.getId());
					i.putExtra("type", user.isType());
					startActivity(i);
				} else {
					Toast.makeText(con, "Invalid email or password",
							Toast.LENGTH_LONG * 1000).show();
				}
			}
		});

		signup.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(con, Signup.class);
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
