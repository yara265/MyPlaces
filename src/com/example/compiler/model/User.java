package com.example.compiler.model;

public class User {

	private int id;

	private String name;

	private String password;

	private String email;

	private boolean type;

	public User() {
	}

	public User(int id, String name, String password, String email, boolean type) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isType() {
		return type;
	}

	public void setType(boolean type) {
		this.type = type;
	}

}
