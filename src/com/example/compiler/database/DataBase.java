package com.example.compiler.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.compiler.model.User;

public class DataBase extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_NAME = "compiler";

	private static final String TABLE_USER = "user";
	private static final String TABLE_FRIENDSHIP = "friend";

	public static final String KEY_ID = "ID";
	public static final String KEY_NAME = "NAME";
	public static final String KEY_EMAIL = "EMAIL";
	public static final String KEY_PASSWORD = "password";
	public static final String KEY_TYPE = "type";

	public static final String KEY_SENDER = "SENDERID";
	public static final String KEY_RECIVER = "RECIVERID";
	public static final String KEY_ACCEPTED = "active";

	public DataBase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "( " + KEY_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT , " + KEY_NAME
				+ " TEXT , " + KEY_EMAIL + " TEXT , " + KEY_PASSWORD
				+ " TEXT , " + KEY_TYPE + " BOOL)";
		db.execSQL(CREATE_USER_TABLE);

		String CREATE_FRIENDSHIP_TABLE = "CREATE TABLE " + TABLE_FRIENDSHIP
				+ "( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
				+ KEY_SENDER + " INTEGER , " + KEY_RECIVER + " INTEGER , "
				+ KEY_ACCEPTED + " BOOL)";
		;
		db.execSQL(CREATE_FRIENDSHIP_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public User login(String email, String password) {

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_USER, new String[] { KEY_NAME, KEY_ID,
				KEY_TYPE }, KEY_EMAIL + "=? and " + KEY_PASSWORD + "=?",
				new String[] { email, password }, null, null, null, null);
		try {
			if (cursor != null)
				cursor.moveToFirst();
			String name = cursor.getString(0);
			int id = cursor.getInt(1);
			boolean b = cursor.getInt(2) == 1;
			cursor.close();

			User user = new User();
			user.setId(id);
			user.setName(name);
			user.setEmail(email);
			user.setPassword(password);
			user.setType(b);

			return user;
		} catch (Exception d) {
			return null;
		}
	}

	public void sign(User user) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues progValues = new ContentValues();
		progValues.put(KEY_NAME, user.getName());
		progValues.put(KEY_EMAIL, user.getEmail());
		progValues.put(KEY_PASSWORD, user.getPassword());
		progValues.put(KEY_TYPE, true);

		db.insert(TABLE_USER, null, progValues);

		db.close();
	}

	public boolean sendFriendRequest(int senderid, int reciverid) {
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues progValues = new ContentValues();
			progValues.put(KEY_SENDER, senderid);
			progValues.put(KEY_RECIVER, reciverid);
			progValues.put(KEY_ACCEPTED, false);
			db.insert(TABLE_FRIENDSHIP, null, progValues);
			db.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public List<User> getProperFriendList(int userid) {

		SQLiteDatabase db = this.getReadableDatabase();
		String condition = KEY_ID + " not in " + "" + "( select " + KEY_SENDER
				+ " from " + TABLE_FRIENDSHIP + " where " + KEY_RECIVER + " = "
				+ userid + " ) and " + KEY_ID + " not in ( select "
				+ KEY_RECIVER + " from " + TABLE_FRIENDSHIP + " where "
				+ KEY_SENDER + " = " + userid + " ) and " + KEY_ID + " != "
				+ userid;

		Cursor cursor = db.query(TABLE_USER, new String[] { KEY_NAME, KEY_ID },
				condition, null, null, null, null, null);

		try {
			List<User> list = new ArrayList<User>();
			if (cursor != null) {
				// cursor.moveToFirst();
				while (cursor.moveToNext()) {
					String name = cursor.getString(0);
					int id = cursor.getInt(1);
					User user = new User();
					user.setId(id);
					user.setName(name);
					list.add(user);
				}
				cursor.close();
			}
			return list;
		} catch (Exception d) {
			return null;
		}
	}

	public List<User> getFriendRequestList(int userid) {

		SQLiteDatabase db = this.getReadableDatabase();
		String condition = KEY_RECIVER + " = " + userid + " and "
				+ KEY_ACCEPTED + " = " + 0;

		Cursor cursor = db.query(TABLE_FRIENDSHIP, new String[] { KEY_SENDER },
				condition, null, null, null, null, null);

		try {
			List<Integer> ids = new ArrayList<Integer>();
			if (cursor != null) {
				// cursor.moveToFirst();
				while (cursor.moveToNext()) {
					int id = cursor.getInt(0);
					ids.add(id);
				}
				cursor.close();
			} else
				return null;

			if (ids.size() == 0)
				return new ArrayList<User>();

			String contidtio2 = "";
			for (int i = 0; i < ids.size(); i++) {
				contidtio2 += KEY_ID + " = " + ids.get(i) + " ";
				if (i != ids.size() - 1)
					contidtio2 += " or ";
			}

			Cursor cursor2 = db.query(TABLE_USER, new String[] { KEY_NAME,
					KEY_ID }, contidtio2, null, null, null, null);

			List<User> list = new ArrayList<User>();
			if (cursor2 != null) {
				// cursor.moveToFirst();
				while (cursor2.moveToNext()) {
					int idd = cursor2.getInt(1);
					String name = cursor2.getString(0);
					User user = new User();
					user.setName(name);
					user.setId(idd);
					list.add(user);

				}

				cursor2.close();
			} else
				return null;

			return list;
		} catch (Exception d) {
			return null;

		}
	}

	public boolean aceptFriendRequest(int reciver, int sender) {
		try {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues progValues = new ContentValues();
			progValues.put(KEY_ACCEPTED, true);

			db.update(TABLE_FRIENDSHIP, progValues, KEY_RECIVER + " = "
					+ reciver + " and " + KEY_SENDER + " = " + sender, null);

			db.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean upgradUserType(int userId) {
		try {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues progValues = new ContentValues();
			progValues.put(KEY_TYPE, false);

			db.update(TABLE_USER, progValues, KEY_ID + " = " + userId, null);

			db.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
