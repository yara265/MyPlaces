package com.example.compiler;

import java.util.Random;

import com.example.compiler.database.DataBase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Logined extends Activity {

	TextView email;
	TextView name;
	Button logout;
	Button sendFriendRequest;
	Button aceptFriendRequest;
	Button choice;
	Context con = this;
	int userId;
	DataBase dp;
	boolean usertype;

	Random rand = new Random();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logined);
		dp = new DataBase(con);
		String emailValue = getIntent().getStringExtra("email");
		String nameValue = getIntent().getStringExtra("name");
		usertype = getIntent().getBooleanExtra("type", true);
		userId = getIntent().getIntExtra("id", 0);
		email = (TextView) findViewById(R.id.lemail);
		email.setText("Your Email " + emailValue);
		name = (TextView) findViewById(R.id.lname);
		name.setText("Hello " + nameValue);
		logout = (Button) findViewById(R.id.llogout);
		logout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		sendFriendRequest = (Button) findViewById(R.id.lsend);
		sendFriendRequest.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(con, SendFriendRequest.class);
				i.putExtra("id", userId);
				startActivity(i);
			}
		});
		aceptFriendRequest = (Button) findViewById(R.id.laccept);
		aceptFriendRequest.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(con, AcceptFriendRequest.class);
				i.putExtra("id", userId);
				startActivity(i);
			}
		});
		choice = (Button) findViewById(R.id.lchoice);
		if (usertype)
			choice.setText("upgrade Account ");
		else
			choice.setText("verify Creditcard ");
		choice.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Intent i = new Intent(con, AcceptFriendRequest.class);
				// i.putExtra("id", userId);
				// startActivity(i);
				if (usertype)
					if (dp.upgradUserType(userId)) {
						Toast.makeText(con, "user upgraded",
								Toast.LENGTH_LONG * 1000).show();
						choice.setText("verify Creditcard ");
						usertype = false;
					} else {
						Toast.makeText(con, "something went wrong",
								Toast.LENGTH_LONG * 1000).show();
					}
				else {
					Toast.makeText(
							con,
							rand.nextBoolean() ? "valid credit card"
									: "not vaild card",
							Toast.LENGTH_LONG * 1000).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logined, menu);
		return true;
	}

}
