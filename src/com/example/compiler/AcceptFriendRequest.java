package com.example.compiler;

import java.util.ArrayList;
import java.util.List;

import com.example.compiler.database.DataBase;
import com.example.compiler.model.User;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AcceptFriendRequest extends ListActivity {

	static String[] Users = null;
	List<User> list = null;
	int userid;
	DataBase dp;
	ArrayAdapter<String> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userid = getIntent().getIntExtra("id", 0);
		dp = new DataBase(this);
		list = dp.getFriendRequestList(userid);
		if (list == null)
			list = new ArrayList<User>();

		Users = new String[list.size()];
		for (int i = 0; i < Users.length; i++) {
			Users[i] = list.get(i).getName();
		}
		adapter = new ArrayAdapter<String>(this, R.layout.list_users, Users);
		setListAdapter(adapter);

		ListView listView = getListView();
		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String user = (String) ((TextView) view).getText();
				int dd = 0;

				for (int i = 0; i < list.size(); i++) {
					if (user.equals(list.get(i).getName())) {
						dd = list.get(i).getId();
						list.remove(i);
						break;
					}
				}
				if (dp.aceptFriendRequest(userid, dd)) {
					Toast.makeText(getApplicationContext(),
							"You are Friend  with " + user, Toast.LENGTH_SHORT)
							.show();
					finish();
				} else {
					Toast.makeText(getApplicationContext(),
							"somethng went wrong", Toast.LENGTH_SHORT).show();
				}

			}
		});

	}
}
